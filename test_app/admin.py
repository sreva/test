from django.contrib import admin
from models import Festival


class FestivalAdmin(admin.ModelAdmin):
    list_display = ('title', 'country', 'date',)

admin.site.register(Festival, FestivalAdmin)