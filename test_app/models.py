# --*-- coding: utf-8 --*--
from django.db import models

# Create your models here.
class Festival(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название')
    country = models.CharField(max_length=200, verbose_name=u'Страна')
    date = models.DateTimeField(verbose_name=u'Дата начала')
