# Create your views here.
import json
from django.http import HttpResponse
from django.views.generic.simple import direct_to_template
from django.core import serializers
from test_app.models import Festival


def festivals(request):
    festival_list = Festival.objects.all()
    if request.POST:
        order = request.POST.get('sort', 'id')
        filt = request.POST.get('filter', False)
        festival_list = festival_list.order_by(order)
        if filt:
            festival_list = festival_list.filter(title__icontains=filt)
        return HttpResponse(serializers.serialize("json", festival_list), mimetype="application/json")
    return direct_to_template(request, 'festival/festivals.html', locals())